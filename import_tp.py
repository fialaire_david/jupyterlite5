#!/usr/bin/env python
# coding: utf-8

"""

Importe un TP de lyceum pour en avoir une version dans jupyterlite.

Parv défaut les TP sont placés dans un dossier _tp dans le répertoire du cours.


"""

from pathlib import Path
import subprocess 
import os
import re
from collections import namedtuple
import argparse

# lien symbolique venant de diapo
from slugifier_link import slugify

# Dossier accueillant les notebooks
CONTENT_DIR = Path("/home/ben/Documents/git/0lyceum/jupyterlite/content/")
# for testing purposes
DEFAULT_TP_DIR = "/home/ben/Documents/git/0lyceum/www/content/courses/tg/nsi/5-Algorithmique/3-Diviser pour régner/_tp/"

parser = argparse.ArgumentParser(
    description="Copie les fichiers du tp dans un dossier accessible par jupyterlite."
)
parser.add_argument(
    "tp_dir",
    metavar="tp_dir",
    type=str,
    nargs="?",
    help="Le nom du cours à traiter",
)

args = parser.parse_args()

if args.tp_dir is not None:
    tp_dir = args.tp_dir
else:
    tp_dir = "/home/ben/Documents/git/0lyceum/www/content/courses/tg/nsi/5-Algorithmique/3-Diviser pour régner/_tp/"

# tp_dir = Path(tp_dir)

def copy_files(tp_dir):
    """Copie les fichiers mis à jour à leur place"""
    out_dir = Path(slugify(re.sub(r".*courses/(.*)_tp/?", r"\1", tp_dir)))
    out_dir = CONTENT_DIR / out_dir 
    # crée le dossier si nécessaire
    if not os.path.exists(out_dir):
        print("Création de réprtoire", out_dir)
        os.makedirs(out_dir)
    subprocess.run(["rsync", "-aP", f"{tp_dir}/", f"{out_dir}/"])

copy_files(tp_dir)
    
